#include <iostream>
#include <initializer_list>
#include <algorithm>
#include <string>
#include <stdexcept>


using namespace std;

class mojNiz{
  
  private:
        size_t vel{0};
        int *poc{nullptr};
        int kap;

  public:

        mojNiz() : vel{0}, poc{nullptr} {};

       int& operator[](int index){
         if ( index > vel-1 ){
           string poruka = "Uneseni index nije validan";
             throw std::invalid_argument(poruka);
         }
         return poc[index];
        }

       mojNiz operator+( const mojNiz& b ){
         mojNiz novi;
         if ( vel != b.vel ){
           string poruka = "Vektori nisu jednake velicine!";
           throw std::invalid_argument(poruka);
         }

else{
         for( auto i=0; i<vel; i++ )
           novi.push_back(poc[i]+b.poc[i]);
       }
       return novi;
}
      mojNiz operator*( const int& br ){
        mojNiz novi;
          for( auto i=0; i<vel; i++ ){
            novi.push_back(poc[i]*br);
      }
      return novi;

}

      mojNiz& operator++(){
        for(auto i=0; i<vel; i++){
          poc[i] = poc[i]+1;
        }
        return *this;
}
       
       void push_back(const int& a){
         
         if( vel == 0 ){
           auto velNew = vel+1;
           auto pocNew = new int[velNew];
           vel = velNew;
           poc = pocNew;
           poc[vel-1] = a;

         }
      else{
         auto velNew = vel+1;
         auto pocNew = new int[velNew];
         copy(poc,poc+vel,pocNew);

         vel = velNew;
         poc = pocNew;

         poc[vel-1] = a;
     
       }
 }

        mojNiz(initializer_list<int> a) : vel{a.size()}, poc{new int[vel]}{
          copy(a.begin(), a.end(), poc);   
        }

        mojNiz(const mojNiz& b): vel{b.vel}, poc{new int[vel]}{
          copy(b.poc,b.poc + b.vel, poc);
        }
        
        mojNiz& operator=(const mojNiz& b){
          if ( this != &b ){
            delete [] poc;
            vel = b.vel;
            poc = new int[vel];
            copy(b.poc, b.poc+vel, poc);
          } 
          return *this;
        }

        mojNiz& operator=(mojNiz&& b){
          delete [] poc;
          poc = b.poc;
          vel = b.vel;
          b.poc = nullptr;
          b.vel = 0;
          return *this;
        }

        mojNiz( mojNiz&& b ): vel{b.vel}, poc{b.poc}{
          b.vel = 0;
          b.poc = nullptr;
        }

        ~mojNiz(){ delete [] poc; }
 
        int size() const { return vel; }
        int& at(size_t index) { 
          if( index > vel-1 ) {
            string poruka = "Uneseni index nije validan";
            throw std::invalid_argument(poruka);
          }
            
          return poc[index]; }

};



int main(){
  mojNiz a{1,2,3};
  mojNiz b;

  b.push_back(5);
  b.push_back(1);
  b.push_back(8);

  mojNiz c = a * 2;
  ++c;
  cout << c[0] << c[1] << c[2] << endl;

  return 0;
      
}
